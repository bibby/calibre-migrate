# calibre-migrate

A small script to transfer content (books) between two local calibre ebook libraries using the command line only.

 
    OPTIONS:        
	-s   Source library path
	-d   Destination library path
	-l   List contents of the source library [default:false]
	-i   Book IDs, comma separated
	-v   Verbose output [default:false]
	-? --help  :  usage

## Workflow

* Identify the IDs of content to migrate using `--list`

        calibre-migrate -s $lib --list
        # this is the same as using calibredb
        calibredb list --with-library=$lib --fields title

* Migrate from source to destination

        calibre-migrate -s $src -d $dest --ids 2,3,4

## Use at your own risk

Inspect the code before you use it, and back up your content!
There is `rm -rf`! Test it out with dummy libraries.

## My use case

This script was made to fit my own use case, which may be quite different than yours. I have multiple calibre libraries; presently 3 libraries on 3 servers.
I use this utility to move content between them. By migrating content to a middle-man library that is [btsync](http://www.bittorrent.com/sync/)'d to the destination server, I migrate a second time from the courier library to its final home. 

## Libraries

This project inclues a copy of [nk412/optparse](https://github.com/nk412/optparse) to parse the command line options.


